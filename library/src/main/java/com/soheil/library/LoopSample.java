package com.soheil.library;

public class LoopSample {

    public static void main(String[] args) {
        int a = 1;

        while (a <= 50) {
            SwitchCase_SampleCode.print(a);
            a++;
        }

        int b = 1;
        do {
            SwitchCase_SampleCode.print("b = " + b);
            b++;
        } while (b < 10);

        for (int i = 0; i < 10; i++) {
            SwitchCase_SampleCode.print("I Is " + i);
        }

    }


}
