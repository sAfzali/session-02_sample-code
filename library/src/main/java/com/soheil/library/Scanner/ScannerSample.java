package com.soheil.library.Scanner;

import com.soheil.library.SwitchCase_SampleCode;

import java.util.Scanner;

public class ScannerSample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter Your Name");
        String name = scanner.next();
        System.out.println("Please Enter Your Family");
        String family = scanner.next();
        System.out.println("Please Enter Your Age");
        int age = scanner.nextInt();
        SwitchCase_SampleCode.print("My Name Is " + "" +
                name + "," + "My Family Is " + "" + family +
                "," + "Age IS" + " " + age);
    }


}
