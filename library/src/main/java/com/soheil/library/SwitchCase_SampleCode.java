package com.soheil.library;

public class SwitchCase_SampleCode {

    public static void main(String[] args) {
        int age = 35;
        switch (age) {
            case 10:
                print("Child");
                break;
            case 15:
                print("TeenAge");
                break;
            case 20:
                print("Young");
                break;
            //For Example
            case 25:
            case 30:
            case 35:
                print("Old");
                break;
            default:
                print("Error");
                break;
        }

    }

    public static void print(String msg) {
        System.out.println(msg);
    }

    public static void print(int msg) {
        System.out.println(msg);
    }
}
