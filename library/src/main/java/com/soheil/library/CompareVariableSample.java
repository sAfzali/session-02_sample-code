package com.soheil.library;

public class CompareVariableSample {

    public static void main(String[] args) {
        test();
    }


    private static void test() {
        int myAge = 25;
        int yourAge = 20;

        if (myAge > yourAge) {
            SwitchCase_SampleCode.print("I Am Greater");
        } else if (yourAge > myAge) {
            SwitchCase_SampleCode.print("You Are Greater");
        } else
            SwitchCase_SampleCode.print("Equals");

        //True False
        boolean isIranian = true;

        if (isIranian == true) {
            //(isIranian)
            SwitchCase_SampleCode.print("Is Iranian = True");
        } else if (!isIranian) {
            //(isIranian == false)
            SwitchCase_SampleCode.print("Is Iranian = False");
        }

        String myCity = "Tehran";
        String yourCity = "tehran";
        //Equals
        if (myCity.equals(yourCity)) {
            SwitchCase_SampleCode.print("Equals");
        } else {
            SwitchCase_SampleCode.print("False");
        }
        //Equals Ignore Case
        if (myCity.equalsIgnoreCase(yourCity)) {
            SwitchCase_SampleCode.print("Equals");
        } else {
            SwitchCase_SampleCode.print("False");
        }
        //Length
        int myCountryLength = myCity.length();
        SwitchCase_SampleCode.print(myCountryLength);

        //To Lower Case
        if (myCity.toLowerCase().equals(yourCity.toLowerCase())) {
            SwitchCase_SampleCode.print("True");
        } else {
            SwitchCase_SampleCode.print("False");
        //To Upper Case
        }
        if (myCity.toUpperCase().equals(yourCity.toUpperCase())) {
            SwitchCase_SampleCode.print("True");
        } else {
            SwitchCase_SampleCode.print("False");
        }


    }


}
