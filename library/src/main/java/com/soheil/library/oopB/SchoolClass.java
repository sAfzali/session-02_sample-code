package com.soheil.library.oopB;

import com.soheil.library.SwitchCase_SampleCode;

public class SchoolClass {
    public static void main(String[] args) {
        StudentPojo std = new StudentPojo();
        std.name = "Soheil";
        std.family = "Afzali";
        std.sportScore = 20;
        std.phisicScore = 17;
        std.mathScore = 15;

        float average = std.getAverage();
        SwitchCase_SampleCode.print("Average IS " + average);

        //new Student Model

    }


}
