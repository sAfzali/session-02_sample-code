package com.soheil.library.oopD;

public class STDUsing {
    public static void main(String[] args) {
        StudentClass std = new StudentClass();
        std.setName("Soheil");
        std.setFamily("Afzali");
        std.setAge(25);
        String title = "My Name IS " + std.getName() +
                "," + "My Family IS " + std.getFamily() +
                "," + "My Age IS " + std.getAge();

        System.out.println(title);
    }


}
