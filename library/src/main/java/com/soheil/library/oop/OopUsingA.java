package com.soheil.library.oop;

import com.soheil.library.SwitchCase_SampleCode;

public class OopUsingA {

    public static void main(String[] args) {
        HumanPojo ali = new HumanPojo();

        ali.name = "Ali";
        ali.family = "Hassani";
        ali.age = 25;

        SwitchCase_SampleCode.print(
                "My Name IS " + ali.name + "," + "My Family IS " +
                        ali.family + "," + "My Age IS " + ali.age);
    }
}
