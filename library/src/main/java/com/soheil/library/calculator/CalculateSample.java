package com.soheil.library.calculator;

import java.util.Scanner;

public class CalculateSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        print("Enter First Number");
        double firstNumber = scanner.nextDouble();
        print("Enter Action");
        String action = scanner.next();
        print("Enter Second Number");
        double secondNumber = scanner.nextDouble();
        double result = getCalculat(firstNumber, secondNumber, action);
        print("Result IS " + result);
    }

    private static double getCalculat(double first, double second, String action) {
        double result = 0;
        switch (action) {
            case "+":
                result = first + second;
                break;
            case "-":
                result = first - second;
                break;
            case "/":
                result = first / second;
                break;
            case "*":
                result = first * second;
                break;
        }
        return result;

    }

    private static void print(String msg) {
        System.out.println(msg);
    }
}
